<?php
include_once('tools.php');
top_mid_part('Rates');
//print_r($_SESSION);

if (isset($_POST['rate'])) {
  //$_SESSION["Total"] = $_POST["Total"];
  //$_SESSION["GST"] = $_POST["GST"];
  $_SESSION["aid"] = $_POST["aid"];

  $_SESSION["Arrival"] = $_POST["Arrival"];

  $_SESSION["Days"] = $_POST["Days"];

  $_SESSION["adults"] = $_POST["adults"];

  $_SESSION["children"] = $_POST["children"];

  header('Location: booking.php');
}
?>

<script type="text/javascript">
let aid = ''
let pickPrice = 0
function getDetails(det,i){
  document.getElementById('pick').innerHTML = i
  document.getElementById('aid').value = i
  document.getElementById('pickPrice').innerHTML = '$'+det
  aid = i; pickPrice = det
}

function submitPick(){
  var i = document.forms['rate']['Days'].value
    if (i == '') {
      alert("Please select number of days");
      return false
    } 
    
    var j = document.forms['rate']['adults'].value
    if (j == '') {
      alert("Please select number of adults");
      return false;
    }
    var l = document.forms['rate']['children'].value
    if (l == '') {
      alert("Please select number of children");
      return false;
    }
}

function calTotal(){
  let days = document.getElementById('Days').value 
  let adult = document.getElementById('adults').value
  let kids = document.getElementById('children').value
  //alert(days + adult + kids)
  if (kids > 1) {
      var ChildPrice = (kids - 1) * 5;
    }
    else if (kids <= 1) {
      ChildPrice = 0;
    }

    if (adult > 2) {
      var adultPrice = (adult - 2) * 10;
    } else if (adult <= 2) {
      adultPrice = 0;
    } 
    
    if(ChildPrice != undefined && adultPrice != undefined ){
      
    var sum = (pickPrice * days) + (ChildPrice) + (adultPrice);
    //alert(sum);
    var gst = sum * 0.1;
    var total = sum * 0.1 + sum;
    document.getElementById('Total').value = total.toFixed(2);
    document.getElementById('GST').value = gst.toFixed(2);
    }
}

</script>


<main>

  <h1 style="text-align:center;color:blue">Rate table</h1>
  <p style="text-align:center">The per night rate includes 2 adults or 1 adult + 1 child.</p>
  <p style="text-align:center">All price exclude GST.</p>
  <div class="columns">
    <ul class="price">
      <li class="header" style="background-color:#800000">USCS</li>
      <li class="grey">$ 32.25 / night</li>
      <li>Extra cost: </li>
      <li>adult $10</li>
      <li>child $5</li>
      <li class="grey"><a href="#modal" class="button" 
      onclick="getDetails(32.25,'USCS')">Choose</a></li>
    </ul>
  </div>

  <div class="columns">
    <ul class="price">
      <li class="header" style="background-color:#20B2AA">UMCS</li>
      <li class="grey">$ 40.50 / night</li>
      <li>Extra cost: </li>
      <li>adult $10</li>
      <li>child $5</li>
      <li class="grey"><a href="#modal" class="button"
      onclick="getDetails(40.50,'UMCS')">Choose</a></li>
    </ul>
  </div>

  <div class="columns">
    <ul class="price">
      <li class="header" style="background-color:#111">PSCS</li>
      <li class="grey">$ 50.25 / night</li>
      <li>Extra cost: </li>
      <li>adult $10</li>
      <li>child $5</li>
      <li class="grey"><a href="#modal" class="button"
      onclick="getDetails(50.25,'PSCS')">Choose</a></li>
    </ul>
  </div>

  <div class="columns">
    <ul class="price">
      <li class="header" style="background-color:#A9A9A9">PMCS</li>
      <li class="grey">$ 60.50 / night</li>
      <li>Extra cost: </li>
      <li>adult $10</li>
      <li>child $5</li>
      <li class="grey"><a href="#modal" class="button"
      onclick="getDetails(60.50,'PMCS')">Choose</a></li>
    </ul>
  </div>
  <div class="columns">
    <ul class="price">
      <li class="header" style="background-color:#00008B">CS</li>
      <li class="grey">$ 100.0 / night</li>
      <li>No extra cost</li>
      <li>adult free</li>
      <li>child free</li>
      <li class="grey"><a href="#modal" class="button"
      onclick="getDetails(100.0,'CS')">Choose</a></li>
    </ul>
  </div>

  <div id="modal">
    <div class="modal__window">
      <a class="modal__close" href="#">Close X</a>
      
      <div class="selection">
    <!-- <h3 id="type">Price Evaluator</h3> -->
    <b style="font-size: 20px;">Accomodation Type: </b> <span id="pick" style="color: red;"></span><br><br>
    <b style="font-size: 20px;">Price per Night: </b><span id="pickPrice" style="color: red;"></span><br>
    
    <form  name="rate" method="post" action=""
    style="text-align:100px" onsubmit="return(submitPick())">
      <br><br><br>
      <span>Arrival Date : </span><input type="date" name="Arrival" id="Arrival" placeholder="dd/mm/yyyy" required /><br>
      <input type="hidden" name="aid" id="aid" value=""  /><br>

      <span>Number of Days : </span><select name="Days" id="Days" onchange="calTotal()" style="padding: 2px;">
        <option value="">Please select</option>
        <?php for ($i = 1; $i <= 14; $i++)
          echo "<option value=$i>$i</option>";
        ?>
      </select><br><br>
      <span>Number of Adults : </span><select name="adults" id="adults" onchange="calTotal()" style="padding: 2px;">
        <option value="">Please select</option>
        <?php for ($j = 1; $j <= 10; $j++)
          echo "<option value=$j>$j</option>";
        ?>
      </select><br><br>
      <span>Number of Children : </span><select name="children" id="children" onchange="calTotal()" style="padding: 2px;">
        <option value="">Please select</option>
        <?php for ($k = 0; $k <= 10; $k++)
          echo "<option value=$k>$k</option>";
        ?>
      </select>

      <div class="total">

        <b>Total $ : </b><output name="Total" id="Total" value="" style="float:right"></output>
        <br>
        <b>Includes GST : </b><output name="GST" id="GST" value="" style="float:right"></output>
        <br><input type="submit" name="rate"  style="margin-top:20px;" >
      </div>
    </form>
  </div>
    </div>
  </div>

  <div>

    <img src="media/creditcard.jpg" alt="payment" class="center" />

  </div>




</main>

<?php

end_part();

?>