<?php
include_once('tools.php');
top_mid_part('Booking');
//print_r($_SESSION);
//var_dump($_SESSION);

error_reporting(0);
//$aid = $_POST['aid'];
$aid = $_SESSION["aid"];
$arrival = $_SESSION['Arrival'];
$days = $_SESSION['Days'];
$adults = $_SESSION['adults'];
$children = $_SESSION['children'];


$site =  array(
  'USCS' => 32.25,
  'PSCS' => 50.25,
  'UMCS' => 40.50,
  'PMCS' => 60.50,
  'CS' => 100,
);
$sitePrice = $site[$aid] * $days;

if ($adults > 2) {
  $adultPrice = ($adults - 2) * 10;
} else {
  $adultPrice = 0;
}
if ($children > 1) {
  $childPrice = ($children - 1) * 5;
} else {
  $childPrice = 0;
}
$sum = $sitePrice + $adultPrice + $childPrice;
$gst = round($sum * 0.1, 2);
$Total = round($sum, 2) + $gst;
$_SESSION["total"] = $Total;
//$_SESSION["gst"] = $gst;

$name_error = $email_error = $phone_error = $address_error = 
$zip_error = $card_error = $exp_error = $ccv_error = "";
$name = $email = $phone = $address = $card = $zip = $exp = $ccv = "";

//form is submitted with POST method
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $name_error = "Name is required.";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
      $name_error = "Only letters and white space allowed";
    }
    $_SESSION["name"] = $_POST["name"];
  }

  if (empty($_POST["email"])) {
    $email_error = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email_error = "Invalid email format";
    }
    $_SESSION["email"] = $_POST["email"];
  }

  if (empty($_POST["phone"])) {
    $phone_error = "Phone is required";
  } else {
    $phone = test_input($_POST["phone"]);
    if (!preg_match("/[0-9]$/", $phone)) {
      $phone_error = "Invalid phone number";
    }
    $_SESSION["phone"] = $_POST["phone"];
  }

  if(empty($_POST["address"])){
    $address_error = "Address is required";
  }else{ 
    $address = test_input($_POST["address"]);
    $_SESSION["add"] = $_POST["address"];
  }

  if(empty($_POST["zip"])){
    $zip_error = "Zip/Postcode is required";
  }else{ 
    $zip = test_input($_POST["zip"]);
  }

  if(empty($_POST["card-num"])){
    $card_error = "Please Enter Card Number!";
  }else{ 
    $card = test_input($_POST["card-num"]);
    if(!preg_match("/[0-9]$/", $card)){
      $card_error = "16 Digits Cards Only";
    }
  }

  if(empty($_POST["expire"])){
    $exp_error = "Please Enter Card Expiry Date";
  }else{ 
    $exp = test_input($_POST["expire"]);
  }

  if(empty($_POST["security"])){
    $ccv_error = "Please Enter CCV Number";
  }else{ 
    $ccv = test_input($_POST["security"]);
    if(!preg_match("/[0-9]{3}$/", $ccv)){
      $ccv_error = "3 Digits CCV Only";
    }
  }

  

  if ($name_error == '' and $email_error == '' and $phone_error == ''
  and $address_error == '' and $zip_error == '' and $card_error == '' 
  and $exp_error == '' and $ccv_error == '') {
    
    // action="receipt.php" target="_blank"
    echo "
    <script type='text/javascript'>
       window.open('receipt.php');
       window.location.href = 'index.php';
    </script>";
    
    //header("Location: receipt.php");
    //exit();
  }
}

function test_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


$imgArray = array(
  'USCS' => "media/2.jpg",
  'PSCS' => "media/7.jpg",
  'UMCS' => "media/8.jpg",
  'PMCS' => "media/6.jpg",
  'CS' => "media/9.jpg",
);

?>

<style>
  div.container {
    max-width: 100%;
    max-height: 100%;
    /* display: inline-block; */
    width: 90%;
  }

  .wrapper input {
    width: 90%;
    padding: 5px;
  }

  .wrapper {
    display: inline-flex;
    position: absolute;
    margin: -560px 0px 10px 530px;
    font-size: 15px;
  }

  .btns button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 5px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: auto;
    cursor: pointer;

  }

  .error {
    color: #FF0000;
  }

  .value {
    color: blue;
  }

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
</style>

  
</>

<h1>
  <i class="fa fa-shipping-fast"></i>
  Booking Details
</h1>
<span class="error" id="alert-msg" style="margin-left: 500px;"></span>
<div class="container">
  <img src=<?php echo $imgArray[$aid]; ?> alt="small unpowered" class="acc" id="book-img" />
  <ul>
    <li><em>Accommodation Type : </em><span class="value"><?php echo $aid; ?></span></li>
    <li><em>Date of Arrival : </em><span class="value"><?php echo $arrival; ?></span></li>
    <li><em>Number of Days : </em><span class="value"><?php echo $days; ?></span></li>
    <li><em>Number of Adults : </em><span class="value"><?php echo $adults; ?></span></li>
    <li><em>Number of Children : </em><span class="value"><?php echo $children; ?></span></li>
    <li><em>Total Price : </em><span class="value"><?php echo $Total; ?></span></li>
    <li><em>Includes GST : </em><span class="value"><?php echo $gst; ?></span></li>
  </ul>


  <div class="wrapper">
    <div>
      <form method="post" 
      
      >
        <h1>
          <i class="fas fa-shipping-fast"></i>
          Reservation:
        </h1>
        <div class="name">
          <div>
            <label for="name"><b>Name: </b></label>
            <span class="error">*<?php echo $name_error;?></span>
            <input type="text" name="name" id="cust-name" value="<?php echo $name;?>"
            placeholder="Billing Name..."><br><br>
          </div>
        </div>
        <div class="phone" style="width: 50%; float:left">
          <label for="phone"><b>Phone: </b></label>
          <span class="error">*<?php echo $phone_error;?></span>
          <input type="number" name="phone" id="cust-phone" value="<?php echo $phone;?>"
          placeholder="Ex: 0412344567">
        </div>
        <div class="email" style="width: 50%;float:left">
          <div>
            <label for="email"><b>Email: </b></label>
            <span class="error">*<?php echo $email_error;?></span>
            <input type="email" name="email" id="cust-email" value="<?php echo $email;?>"
            placeholder="email@xxxx.xxx"><br><br>
          </div>
        </div>
        <div class="address">
          <div>
            <label for="address"><b>Address: </b></label>
            <span class="error">*<?php echo $address_error;?></span>
            <input type="text" name="address" id="cust-add" value="<?php echo $address;?>"
            placeholder="e.g: 283 city road, Southbank"><br><br>
          </div>
        </div>
        <div class="zip" style="width: 50%;float:left">
          <div>
            <label for="zip"><b>Zip/Post Code: </b></label>
            <span class="error">*<?php echo $zip_error;?></span>
            <input type="text" name="zip" id="cust-zip" value="<?php echo $zip;?>"
            placeholder="0000">
          </div>
        </div><br>
        <h1 style="display: inline-block;">
          <i class="far fa-credit-card"></i> Payment Information
        </h1>
        <div class="cc-num">
          <label for="card-num"><b>Credit Card No.: </b></label>
          <span class="error">*<?php echo $card_error;?></span>
          <input type="number" name="card-num" id="cust-card" value="<?php echo $card;?>"
          placeholder="Card Number"><br><br>
        </div>
        <div class="exp" style="width: 50%;float:left">
          <div>
            <label for="card-num"><b>Exp: </b></label>
            <span class="error">*<?php echo $exp_error;?></span>
            <input type="date" name="expire" id="cust-exp" value="<?php echo $exp;?>">
          </div>
        </div>
        <div class="ccv" style="width: 50%;float:left">
          <div>
            <label for="card-num"><b>CCV: </b></label>
            <span class="error">*<?php echo $ccv_error;?></span>
            <input type="password" name="security" id="cust-ccv" value="<?php echo $ccv;?>"
            placeholder="123"><br><br>
          </div>
        </div>
        <div class="btns">
          <button type="submit" name="booking">Confirm</button>
          <button type="reset" onclick="location.href = 'accommodation.php';">Go Back</button>

        </div>
      </form>
    </div>
  </div>
</div>

<?php

end_part();

?>


<?php
$errorMessage = "";
if ($errorMessage != "") {
  echo ("<p>There was an error:</p>\n");
  echo ("<ul>" . $errorMessage . "</ul>\n");
} else {
  $fs = fopen("bookings.txt.tsv", "a");
  fwrite($fs, $name . ", " . $email . ", " . $phone . ", " . $address . ", " . $aid . ", " . $arrival . ", " . $days . ", " . $adults . ", " . $children . ", " . $Total . "\n");
  fclose($fs);
  exit;
}
?>
