<?php
include_once('tools.php');
top_mid_part('Contact Us');

$name_error = $email_error = $phone_error = $message_error = "";
$name = $email = $phone = $message = $subject = $success = $msg = "";

//form is submitted with POST method
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $name_error = "Name is required.";
  } else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
      $name_error = "Only letters and white space allowed";
    }
  }

  if (empty($_POST["email"])) {
    $email_error = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email_error = "Invalid email format";
    }
  }

  if (empty($_POST["phone"])) {
    $phone_error = "Phone is required";
  } else {
    $phone = test_input($_POST["phone"]);
    // check if e-mail address is well-formed
    if (!preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i", $phone)) {
      $phone_error = "Invalid phone number";
    }
  }

  if(($_POST["subject"])){
    $subject = test_input($_POST["subject"]);
  }

  if(empty($_POST["message"])){
    $message_error = "Message is required";
  }else{
    $msg = test_input($_POST["message"]);
  }

  

  if ($name_error == '' and $email_error == '' 
  and $phone_error == '' and $message_error == '') {
    $message = "<h2 style='color: green;'>Message Send! Thanks for contacting us</h2>";
    echo "$message";
  }
}

function test_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


?>



<h1> Contact Us</h1>
<div class="container" >
  <fieldset>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" 
    onsubmit=" return validateForm()" id="contact-form">

      <div class="form-group">
        <label for="contact-name"><b>Full Name</b></label>
        <input type="text" class="form-control" id="contact-name" value="" 
        name="name" placeholder="Enter your name.." onkeyup='validateName()'>
        <span class='error' id='name-error'><?= $name_error ?></span><br>
      </div>

      <div class="form-group">
        <label for="contact-phone"><b>Phone Number</b></label>
        <input type="text" class="form-control" id="contact-phone" value="" 
        name="phone" placeholder="Ex: 0412344567" onkeyup='validatePhone()'>
        <span class='error' id='phone-error'><?= $phone_error ?></span>
      </div>

      <div class="form-group">
        <label for="contact-email"><b>Email address</b></label>
        <input type="email" class="form-control" id="contact-email" 
        name="email" placeholder="email@xxxx.xxx" onkeyup='validateEmail()'>
        <span class='error' id='email-error'><?= $email_error ?></span>
      </div>

      <div class="form-group">
        <label for="contact-subject"><b>Subject</b></label>
        <input type="text" class="form-control" id="contact-subject" 
        name="subject" placeholder="Enter Subject" onkeyup='validateEmail()'>
        <span class='error' id='sub-error'></span>
      </div>


      <div class="form-group">
        <label for='contactMessage'><b>Message</b></label>
        <textarea class="form-control" rows="5" id='contact-message' 
        value="<?= $message ?>" name="message" placeholder="How can we help you...." 
        onkeyup='validateMessage()'></textarea>
        <span class='error' id='message-error'><?= $message_error ?></span>
      </div>
      <span><b>Sign up with mailing list</b></span><input type="checkbox" name="subscribe" /><br>
      <span><b>Remember Me</b></span><input type="checkbox" id="remember" name="remember" />
      <input class="submit" name="submit" type="submit" value="Submit" onclick='storedValue()' style="float:right">


      <span class='error' id='submit-error'></span>
    </form>
  </fieldset>
</div>

<?php

end_part();

?>


<script>
  function validateName() {

    var name = document.getElementById('contact-name').value;

    if (name.length == 0) {

      producePrompt('Name is required', 'name-error', 'red')
      return false;

    }

    if (!name.match(/^[A-Za-z]*\s{1}[A-Za-z]*$/)) {

      producePrompt('First and last name, please.(letters only)', 'name-error', 'red');
      return false;

    }

    producePrompt('Valid', 'name-error', 'green');
    return true;

  }

  function validatePhone() {

    var phone = document.getElementById('contact-phone').value;

    if (phone.length == 0) {
      producePrompt('Phone number is required.', 'phone-error', 'red');
      return false;
    }

    if (phone.length > 10) {
      producePrompt('10 Digits Phone Numbers Only.', 'phone-error', 'red');
      return false;
    }

    if (!phone.match(/^04[0-9]{8}$/)) {
      producePrompt('Include area code(04) & Only digits, please.', 'phone-error', 'red');
      return false;
    }

    producePrompt('Valid', 'phone-error', 'green');
    return true;

  }

  function validateEmail() {

    var email = document.getElementById('contact-email').value;

    if (email.length == 0) {

      producePrompt('Email Invalid', 'email-error', 'red');
      return false;

    }

    if (!email.match(/^[A-Za-z\._\-[0-9]*[@][A-Za-z]*[\.][a-z]{2,4}$/)) {

      producePrompt('Email Invalid', 'email-error', 'red');
      return false;

    }

    producePrompt('Valid', 'email-error', 'green');
    return true;

  }

  function validateMessage() {
    var message = document.getElementById('contact-message').value;

    if (message.length == 0 || message.length < 15) {
      producePrompt('Please enter at least a sentence', 'message-error', 'red');
      return false;
    }

    producePrompt('Valid', 'message-error', 'green');
    return true;

  }

  function validateForm() {
    if (!validateName() || !validatePhone() || !validateEmail() || !validateMessage()) {
      jsShow('submit-error');
      producePrompt('Please enter the missing field values.', 'submit-error', 'red');
      setTimeout(function() {
        jsHide('submit-error');
      }, 2000);
      return false;
    } else {
      return true;
    }
  }

  function jsShow(id) {
    document.getElementById(id).style.display = 'block';
  }

  function jsHide(id) {
    document.getElementById(id).style.display = 'none';
  }

  function producePrompt(message, promptLocation, color) {

    document.getElementById(promptLocation).innerHTML = message;
    document.getElementById(promptLocation).style.color = color;
  }

  function storedValue() {
    if (document.getElementById('remember').checked) {
      var contactName = document.getElementById('contact-name').value;
      localStorage.setItem('contact-name', contactName);
      var contactPhone = document.getElementById('contact-phone').value;
      localStorage.setItem('contact-phone', contactPhone);
      var contactEmail = document.getElementById('contact-email').value;
      localStorage.setItem('contact-email', contactEmail);
    }
  }

  window.addEventListener('load', ()=>{
    loadSave()
  })

  function loadSave() {
    var storedName = localStorage.getItem('contact-name');
    //alert(storedName)
    if (storedName) {
      document.getElementById('contact-name').value = storedName;
    }
    var storedPhone = localStorage.getItem('contact-phone');
    if (storedPhone) {
      document.getElementById('contact-phone').value = storedPhone;
    }
    var storedEmail = localStorage.getItem('contact-email');
    if (storedEmail) {
      document.getElementById('contact-email').value = storedEmail;
    }
  }
</script>

<?php
$errorMessage = "";
if ($errorMessage != "") {
  echo ("<p>There was an error:</p>\n");
  echo ("<ul>" . $errorMessage . "</ul>\n");
} else {
  $fs = fopen("mailing.txt.tsv", "a");
  fwrite($fs, $name.", ".$email.", ".$phone.", ".$subject.", ".$msg.  "\n");
  fclose($fs);
  exit;
}
?>