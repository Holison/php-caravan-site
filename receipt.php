<?php
session_start();
//print_r($_SESSION);


 ?>
<html>
	<head>
		<meta charset="utf-8">
		<title>Invoice</title>
		<link rel="stylesheet" href="receipt.css">
		<script src="script.js"></script>
	</head>
	<body>
		<header>
			<h1>Invoice</h1>
			<address >
				<p><?= $_SESSION["name"] ?></p>
				<p><?= $_SESSION["add"] ?></p>
				<p><b>Email: </b><?= $_SESSION["email"] ?></p>
				<p><b>Phone: </b><?= $_SESSION["phone"] ?></p>
				
			</address>
			<span><img alt="" src="media/logo.jpg"></span>
		</header>
		<article>
			<h1>Recipient</h1>
			<address >
				<p>WaterWorld Caravan Park<br>Sales Deparment</p>
			</address>
			<table class="meta">
				<tr>
					<th><span >Invoice #</span></th>
					<td><span ><?php echo(rand(10000,1000000)); ?></span></td>
				</tr>
				<tr>
					<th><span >Date</span></th>
					<td><span ><?php echo date("d/m/Y") ?></span></td>
				</tr>
				<tr>
					<th><span >Amount Due</span></th>
					<td><span id="prefix" >$</span><span><?= $_SESSION["total"] ?></span></td>
				</tr>
			</table>
			<table class="inventory">
				<thead>
					<tr>
						<th><span >AID</span></th>
						<th><span >Arrival</span></th>
						<th><span >Days</span></th>
						<th><span >Adults</span></th>
						<th><span >Children</span></th>
						<!-- <th><span >Price</span></th> -->
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><a class="cut"></a><span ><?= $_SESSION["aid"] ?></span></td>
						<td><span ></span><?= $_SESSION["Arrival"] ?></td>
						<td><span ><?= $_SESSION["Days"] ?></span></td>
						<td><span ></span><span ><?= $_SESSION["adults"] ?></span></td>
						<td><span ></span><?= $_SESSION["children"] ?></td>
						<!-- <td><span >$</span><span><?= 2 ?></span></td> -->
					</tr>
				</tbody>
			</table>

			<table class="balance">
				<tr>
					<th><span >Total</span></th>
					<td><span >$</span><span><?= $_SESSION["total"]  ?></span></td>
				</tr>
				<tr>
					<th><span >Amount Paid</span></th>
					<td><span >$</span><span ><?= $_SESSION["total"]  ?></span></td>
				</tr>
				<tr>
					<th><span >Balance Due</span></th>
					<td><span >$</span><span>0.00</span></td>
				</tr>
			</table>
		</article>
    <p style="float:right;font-size:10px;margin-top:-45px"> Price includes GST</p>
		<aside>
      <br><br><br>
			<h1><span >Additional Notes</span></h1>
			<div >
				<p style="text-align:center">A finance charge of 1.5% will be made on unpaid balances after 30 days.</p>
			</div>
		</aside>
    <script>

</script>
	</body>
</html>
