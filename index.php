<?php
include_once('tools.php');
top_mid_part('Home Page');

?>

<main>

  <article id='Web'>

    <div class="Slideshow">

      <!-- Full-width images with number and caption text -->
      <div class="Slides">
        <div class="numbertext">1 / 3</div>
        <img src="media/site.jpg" width="100%" height="450px">
      </div>

      <div class="Slides">
        <div class="numbertext">2 / 3</div>
        <img src="media/kid.jpg" width="100%" height="450px">
      </div>

      <div class="Slides">
        <div class="numbertext">3 / 3</div>
        <img src="media/4.jpg" width="100%" height="450px">
      </div>

      <!-- Next and previous buttons -->
      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
    <br>

    <!-- The dots/circles -->
    <div style="text-align:center">
      <span class="dot" onclick="currentSlide(1)"></span>
      <span class="dot" onclick="currentSlide(2)"></span>
      <span class="dot" onclick="currentSlide(3)"></span>
    </div>
    <style>
      iframe {
        float: right;
        margin-right: 100px;

      }
    </style>

    <div class="map">
      <p style="display:inline-block;text-align:middle">
        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16954.793808117913!2d144.63815512748974!3d-38.117793508556765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad42e5463d5facb%3A0x5045675218ce1e0!2sPortarlington+VIC+3223!5e0!3m2!1sen!2sau!4v1524624985387" width="600" height="200" frameborder="0" style="border:0"  allowfullscreen></iframe> -->
        <iframe width="500" height="200" frameborder="0" src="https://cn.bing.com/maps/embed?h=400&w=500&cp=37.48529756880875~121.56816296386718&lvl=10&typ=d&sty=r&src=SHELL&FORM=MBEDV8" scrolling="no">
        </iframe>

        <b>PARK LOCATION :</b>
        Portarlington is located on the Bellarine Peninsula
        about a 30-minute drive from Geelong,<br>
        or approximately 90 minutes from Melbourne.<br>
        As you travel towards Portarlington from Geelong <br>
        you will discover why Portarlington is considered the<br>
        "Jewel of the Crown" in Victoria.<br>
        Located in some of Australia’s most popular holiday destinations,
        from the ski fields to the beach and everywhere in between,
        we offer a range of accommodation to suit everyone</p>

      <div style="white-space: nowrap; text-align: end; width: 850px; padding: 6px 0;">
        <a id="largeMapLink" target="_blank" href="https://cn.bing.com/maps?cp=37.48529756880875~121.56816296386718&amp;sty=r&amp;lvl=10&amp;FORM=MBEDLD">View Larger Map</a> &nbsp; | &nbsp;
        <a id="dirMapLink" target="_blank" href="https://cn.bing.com/maps/directions?cp=37.48529756880875~121.56816296386718&amp;sty=r&amp;lvl=10&amp;rtp=~pos.37.48529756880875_121.56816296386718____&amp;FORM=MBEDLD">Get Directions</a>
      </div>
    </div>
  </article>

  <section vertical-align="center" style="display: inline-flex;" id="art-list">
      <div class="details" style="margin-left:80px;">
        <h3 id="details">Facilities</h3>
        <p id="text"> Our facilities include picnic areas,hiking grounds, lakes, forestry and family friendly camping grounds
          which carter for the ends of all age groups</p>
        <button id="moreBtn">View</button>
      </div>

      <div class="details">
        <h3 id="details">Accommodation</h3>
        <p id="text"> Accommodation types inlcudes : small unpowered, small powered,medium unpowered,medium powered, caravan site</p>
        <button id="moreBtn">View</button>
      </div>

      <div class="details">
        <h3 id="details">Ferry Travel</h3>
        <p id="text"> Port Phillip Ferries runs 3 services a day in each direction to
          and from Portarlington and Docklands,</p>
        <button id="moreBtn">Read More</button>
      </div>

      <div class="details">
        <h3 id="details">Car Travel</h3>
        <p id="text"> The park is accessible from Geelong via Bellarine Hwy/B110 and
          from Mornington via the Mornington Peninsula Fwy/M11 </p>
        <button id="moreBtn">Read More</button>
      </div>
    </section>
</main>
<br>
<?php

end_part();

?>


<script>
  var slideIndex = 1;
  showSlides(slideIndex);

  // Next/previous controls
  function plusSlides(n) {
    showSlides(slideIndex += n);
  }

  // Thumbnail image controls
  function currentSlide(n) {
    showSlides(slideIndex = n);
  }

  function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("Slides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
      slideIndex = 1
    }
    if (n < 1) {
      slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
  }

  let counter = 0
  function autoShow(){
    setTimeout(autoShow, 3000)
    var slides = document.getElementsByClassName("Slides");
    var dots = document.getElementsByClassName("dot");

      for (let i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }

    if(counter <= slides.length){
      slides[counter].style.display = "block";
      dots[counter].className += " active";
      counter++;
      if(counter == slides.length){counter = 0}
    }
  };
  autoShow()
  
</script>