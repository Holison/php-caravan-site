<?php
include_once('tools.php');
top_mid_part('Accommodation');
//print_r($_SESSION);

if (isset($_POST['book'])) {
  
  //$_SESSION["Total"] = $_POST["Total"];
  //$_SESSION["GST"] = $_POST["GST"];

  $_SESSION["aid"] = $_POST["aid"];

  $_SESSION["Arrival"] = $_POST["Arrival"];

  $_SESSION["Days"] = $_POST["Days"];

  $_SESSION["adults"] = $_POST["adults"];

  $_SESSION["children"] = $_POST["children"];

  header('Location: booking.php');
}
?>




<script type="text/javascript">
  function checkForm() {
    
    var i = document.forms['booking']['aid'].value
    if (i == '') {
      alert("Please select a camp site");
      return false
    } 
    
    var j = document.forms['booking']['Days'].value
    if (j == '') {
      alert("Please select number of days");
      return false;
    }
    
    var k = document.forms['booking']['adults'].value
    if (k == '') {
      alert("Please select number of adults");
      return false;
    }
    
    var l = document.forms['booking']['children'].value
    if (l == '') {
      alert("Please select number of children");
      return false;
    }

    // if((k + l) > 10){
    //   alert("maximum number of people is 10");
    //   k = 0; l = 0
    //   return false;
    // }

  }

  function mySelection(btn) {

    unset();
    btn.style.background = "#4CAF50";
    btn.style.color = "black";
    if (document.getElementById('aid').value == btn.value) {
      unset();
      document.getElementById('aid').value = '';
    } else {
      document.getElementById('aid').value = btn.value;
      total();
    }

  }

  function unset() {
    document.getElementById('but1').style.background = "#ddd";
    document.getElementById('but2').style.background = "#ddd";
    document.getElementById('but3').style.background = "#ddd";
    document.getElementById('but4').style.background = "#ddd";
    document.getElementById('but5').style.background = "#ddd";
  }



  var site = new Array();
  site['USCS'] = 32.25;
  site['PSCS'] = 50.25;
  site['UMCS'] = 40.50;
  site['PMCS'] = 60.50;
  site['CS'] = 100;

  function getSitePrice() {
    var sitePrice = 0;

    sitePrice = site[document.getElementById('aid').value];

    return sitePrice;

  }


  function total() {

    var children = parseInt(document.getElementById('children').value);

    if (children > 1) {
      var ChildPrice = (children - 1) * 5;
    }
    if (children <= 1) {
      ChildPrice = 0;
    }
    
    var adults = parseInt(document.getElementById('adults').value);

    if (adults > 2) {
      var adultPrice = (adults - 2) * 10;
    } else if (adults <= 2) {
      adultPrice = 0;
    } else {
      return false;
    }

    var dayPrice = parseInt(document.getElementById('Days').value);


    if(ChildPrice != undefined && adultPrice != undefined && getSitePrice() != undefined){
      //console.log(getSitePrice())
    var sum = (getSitePrice() * dayPrice) + (ChildPrice) + (adultPrice);
    //alert(sum);
    var gst = sum * 0.1;
    var total = sum * 0.1 + sum;
    document.getElementById('Total').value = total.toFixed(2);
    document.getElementById('GST').value = gst.toFixed(2);
    }

  }
</script>


<main>

  <!-- <div class="sidebar">
    <h2 style="color:green;margin:20px;"> Events</h2>
    <ul>
      <li><a href="#" class="links"> Link 1</a><br></li>
      <li><a href="#" class="links"> Link 2</a><br></li>
      <li><a href="#" class="links"> Link 3</a><br></li>
      <li><a href="#" class="links"> Link 4</a><br></li>
    </ul>
  </div> -->


  <div id="container">
    <div class="accommo">
      <button type="button" id="but1" name="opt" value="USCS" onclick="mySelection(this)">
        <h3 id="type">Small Unpowered</h3>
        <p id="text"> <b>Details:</b> This site
          provides camp spaces for a whole family.<br>
          <b>Price :</b> $32.25/ Night</p>
        <img src="media/2.jpg" alt="small unpowered" class="acc" />
      </button>
    </div>

    <div class="accommo">
      <button type="button" id="but2" name="opt" value="PSCS" onclick="mySelection(this)">
        <h3 id="type">Small Powered</h3>
        <p id="text"> <b>Details:</b> This site
          provides camp spaces for a whole family.<br>
          <b>Price :</b> $50.25/ Night</p>
        <img src="media/7.jpg" alt="small unpowered" class="acc" />
      </button>
    </div>

    <div class="accommo">
      <button type="button" id="but3" name="opt" value="UMCS" onclick="mySelection(this)">
        <h3 id="type" style="margin-top:5px">Medium Unpowered</h3>
        <p id="text"> <b>Details:</b> This site
          provides camp spaces for a whole family.<br>
          <b>Price :</b> $40.50/ Night</p>
        <img src="media/8.jpg" alt="small unpowered" class="acc" />
      </button>
    </div>

    <div class="accommo">
      <button type="button" id="but4" name="opt" value="PMCS" onclick="mySelection(this)">
        <h3 id="type">Medium Powered</h3>
        <p id="text"> <b>Details:</b> This site
          provides camp spaces for a whole family.<br>
          <b>Price :</b> $60.50/ Night</p>
        <img src="media/6.jpg" alt="small unpowered" class="acc" />
      </button>
    </div>

    <div class="accommo">
      <button type="button" id="but5" name="opt" value="CS" onclick="mySelection(this)">
        <h3 id="type">Caravan Site</h3>
        <p id="text"> <b>Details:</b> This site
          provides camp spaces for a whole fammily.<br>
          <b>Price :</b> $100/ Night</p>
        <img src="media/9.jpg" alt="small unpowered" class="acc" />
      </button>
    </div>
  </div>


  <div class="selection">
    <h3 id="type">Price Evaluator</h3>
    <span id="msg" style="color: red;"></span>
    
    <form  name="booking" method="post" action=""
    style="text-align:100px" onsubmit="return(checkForm())">

      <span>Arrival Date : </span><input type="date" name="Arrival" id="Arrival" placeholder="dd/mm/yyyy" required /><br>
      <input type="hidden" name="aid" id="aid" value="" onchange="getSitePrice()" /><br>

      <span>Number of Days : </span><select name="Days" id="Days" onchange="total()" style="padding: 2px;">
        <option value="">Please select</option>
        <?php for ($i = 1; $i <= 14; $i++)
          echo "<option value=$i>$i</option>";
        ?>
      </select><br><br>
      <span>Number of Adults : </span><select name="adults" id="adults" onchange="total()" style="padding: 2px;">
        <option value="">Please select</option>
        <?php for ($j = 1; $j <= 10; $j++)
          echo "<option value=$j>$j</option>";
        ?>
      </select><br><br>
      <span>Number of Children : </span><select name="children" id="children" onchange="total()" style="padding: 2px;">
        <option value="">Please select</option>
        <?php for ($k = 0; $k <= 10; $k++)
          echo "<option value=$k>$k</option>";
        ?>
      </select>

      <div class="total">

        <b>Total $ : </b><output name="Total" id="Total" value="" style="float:right"></output>
        <br>
        <b>Includes GST : </b><output name="GST" id="GST" value="" style="float:right"></output>
        <br><input type="submit" name="book"  style="margin-top:20px;" >
      </div>
    </form>
  </div>

</main>

<?php

end_part();

?>